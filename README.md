	 ██████╗ █████╗ ██╗      ██████╗    ██╗   ██╗ ██╗    ██████╗ 
	██╔════╝██╔══██╗██║     ██╔════╝    ██║   ██║███║   ██╔═████╗
	██║     ███████║██║     ██║         ██║   ██║╚██║   ██║██╔██║
	██║     ██╔══██║██║     ██║         ╚██╗ ██╔╝ ██║   ████╔╝██║
	╚██████╗██║  ██║███████╗╚██████╗     ╚████╔╝  ██║██╗╚██████╔╝
	╚═════╝╚═╝  ╚═╝╚══════╝ ╚═════╝      ╚═══╝   ╚═╝╚═╝ ╚═════╝


  **Download and Installation :**
	
	
1. [Open this link to access repository and download Go Calc](http://51.91.133.234:32691/#browse/browse:Go_Calc)
2. Select the last tag in the tree and download the one that match with the architecture and the OS of your computer.
3. If necessary, make it executable (example on linux : chmod +x <binary_name>)
4. Now you can use it from a terminal (example : ./<binary_name> "10+10*50/2")
5. (optional) Add it into your PATH to use it from everywhere

  **Usage :**

    calc    ["your_calculation"] ex : calc "40+2"
            [-v] [-version] [--version] 
            [-h] [-help] [--help]

    Here are operators you can use : + - / * ^ e ()

  **Limits of CALC V1.0 :**

    Max number : 1.7976931348623157e+308
    Min number : -1.7976931348623157e+308
    Decimal number precision : 13 digits
            
    When using the sign "^" you need to add parentheses :
    example : "10+10^10" needs to be written : "10+(10^10)"

  **OS and Architectures available :**
  *  -linux-amd64
  *  -windows-amd64
  *  -windows-386
  *  -darwin-amd64
  *  -dragonfly-amd64
  *  -freebsd-amd64
  *  -freebsd-arm5
  *  -freebsd-arm6
  *  -freebsd-arm7
  *  -linux-386
  *  -linux-arm5
  *  -linux-arm6
  *  -linux-arm7
  *  -linux-mips64
  *  -linux-mips64le
  *  -linux-ppc64
  *  -linux-ppc64le
  *  -netbsd-amd64
  *  -netbsd-arm5
  *  -netbsd-arm6
  *  -netbsd-arm7
  *  -openbsd-amd64
  *  -plan9-386
  *  -plan9-amd64
  *  -solaris-amd64

  **Launch Tests (on linux) :**
  
    Download project in the go directory and move into it to launch this command :

    cd ./scripts/
    chmod +x ./go_test_units.sh
    ./go_test_units.sh
    
    The tests are starting.

  **Create web page to get a HTML coverage report (on linux) :**

    Download project in the go directory and move into it to launch these commands :

    cd ./scripts/
    chmod +x ./go_test_coverage_report.sh
    ./go_test_coverage_report.sh

    The report has been created in ../generated/reports/coverage.html

  **Launch Benchmarks (on linux) :**

    Download project in the go directory and move into it to launch these commands :
  
    cd ./scripts/
    chmod +x ./go_test_benchmark.sh
    ./go_test_benchmark.sh
    
    The benchmarks are starting and give the results in the terminal

  **Build the Sources (on linux) :**
  
    Download project in the go directory and move into it to launch these commands :
    
    cd ./scripts/
    chmod +x ./go-build-all.sh
    ./go-build-all.sh ../app/ ../generated/binaries/

    The binary of Go Calc has been created for multiple platforms

  **Upload the binaries on Nexus (on linux) :**

    Build the sources, go to script folder and launch these commands :

    cd ./scripts/
    chmod +x ./go-upload-all-on-nexus.sh
    ./go-upload-all-on-nexus.sh <pathToDirectory> <user> <password> <protocol> <nexusHost> <nexusRepository> <nexusPathDestination>
     
     <pathToDirectory> : The path to the binaries to uploade. In this case, always ../generated/binaries/
     <user> : Your nexus username 
     <password> : Your nexus password
     <protocol> : The transfert protocol (http or https)
     <nexusHost> : The nexus host or ip and port (<ip>:<port>)
     <nexusRepository> : The name of your repository on nexus
     <nexusPathDestination> : The path inside the repository. it will create the folders automatically
     
     example : ./go-upload-all-on-nexus.sh ../generated/binaries/ admin password http nexus.example.net Go_Calc master

    All the content of ../generated/binaries/ has been uploaded to nexus repository
