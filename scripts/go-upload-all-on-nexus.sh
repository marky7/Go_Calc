#!/bin/sh
#
# To use:
#
#   * An example of command to launch the upload is :
#   * ./go-upload-all-on-nexus.sh pathToDirectory user password http myNexusHost myNexusRepository theNexusPathDestination
#
# Features:
#
#   * Upload files from directory to a Nexus Repository 
#
# Requirements:
#
#   * GoLang 1.6+ (for mips and ppc), 1.5 for non-mips/ppc.
#
# 

# Get every params from command
DIRECTORY_PATH=${1}
USER=${2}
PASSWORD=${3}
PROTOCOL=${4}
HOST_NEXUS=${5}
REPOSITORY_NEXUS=${6}
PATH_NEXUS_DESTINATION=${7}

# Loop on every file in directory selected in order to upload them on Nexus Repository
for entry in "$DIRECTORY_PATH"/*
do
  echo "$entry"
  FILE_NAME=${entry##*/}
  eval curl -v -u $USER:$PASSWORD --upload-file $entry $PROTOCOL://$HOST_NEXUS/repository/$REPOSITORY_NEXUS/$PATH_NEXUS_DESTINATION/$FILE_NAME
done


