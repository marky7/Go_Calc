#!/bin/sh
#
# GoLang snippet for generating an HTML coverage report :
#
# To use:
#
#   * ./go_test_coverage_report.sh
#
# Features:
# 
#   * Generate an html report with the test coverage of Go_Calc
#

REPORT_NAME=coverage.html
ROOT_PATH=..
OUTPUT_PATH=../generated/reports

go test -cover $ROOT_PATH/... -coverprofile=$OUTPUT_PATH/c.out
go tool cover -html=$OUTPUT_PATH/c.out -o $OUTPUT_PATH/$REPORT_NAME
rm $OUTPUT_PATH/c.out

echo "The report $REPORT_NAME has been generated at the following location : $PWD/$OUTPUT_PATH/$REPORT_NAME"
