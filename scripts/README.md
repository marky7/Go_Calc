# `/scripts`

Scripts to perform various build, install, analysis, etc operations.

These scripts keep the root level Makefile small and simple.

Examples:

* https://github.com/kubernetes/helm/tree/master/scripts
* https://github.com/cockroachdb/cockroach/tree/master/scripts
* https://github.com/hashicorp/terraform/tree/master/scripts

###### DEV DEPENDENCIES ######
go get -u github.com/davecgh/go-spew/spew
# -u flag, which instructs go get to obtain the package and its dependencies, or update those dependencies

###### BUILDING ######
# Building app binaries from building go-build-all.sh script
./go-build-all.sh ../app/ ./

###### TESTING ######
# Give details from test
go test -v

# Generating an HTML coverage report
go test -cover
go test -cover -coverprofile=c.out
go tool cover -html=c.out -o coverage.html

###### UPLOADING ON NEXUS ######
# Upload files from directory to a Nexus Repository from go-upload-all-on-nexus.sh script
./go-upload-all-on-nexus.sh pathToDirectory user password http myNexusHost myNexusRepository theNexusPathDestination

# Uploading file from curl command 
curl -v -u <user>:<password> --upload-file <mybinary> http://51.91.133.234:32691/repository/<myrepo>/<myfolder>/<mybinaryname>

