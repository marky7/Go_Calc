#!/bin/sh
#
# GoLang snippet for launching unit tests of Go_Calc :
#
# To use:
#
#   * ./go_test_units.sh
#
# Features:
#
#   * Launch unit tests and give details about results
#

go test -v ../...
