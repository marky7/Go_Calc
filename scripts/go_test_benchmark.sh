#!/bin/sh
#
# GoLang snippet for launching benchmark tests in Go_Calc :
#
# To use:
#
#   * ./go_test_benchmark.sh
#
# Features:
#
#   * Launch benchmark tests and give details about results
#

go test -bench=. ../... -benchtime 1s
