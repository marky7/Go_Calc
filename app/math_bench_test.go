package main

import (
	"fmt"
	"testing"
)

// BenchmarkMathOperations benchmark the main mathematical functions (Add, Sub, Quo, Mul and Pow)
// This benchmark compare the main mathematical functions with same input values
func BenchmarkMathOperations(b *testing.B) {
	operations := []struct {
		name string
		fun  func(float64, float64) float64
	}{
		{"Add", Add},
		{"Sub", Sub},
		{"Quo", Quo},
		{"Mul", Mul},
		{"Pow", Pow},
	}
	for _, curOp := range operations {

		tables := []struct {
			x float64
			y float64
		}{
			// Basic numbers test
			{1, 1},
			{22, 20},
			{50, 252},
			{100, 15200},

			// Decimal numbers test
			{1.111, 1.222},
			{2.333, 2.777},
			{2.000, -2.000},
			{0.01109, 0.01109},

			// Big and Small numbers test
			{10e20, 10e20},
			{-10e20, -10e20},
			{-22e40, +22e40},
			{-22e40, 0.0111000},
		}

		for _, table := range tables {
			val1 := table.x
			val2 := table.y

			b.Run(fmt.Sprintf(" %s / val1 : %f and val2 : %f", curOp.name, val1, val2), func(b *testing.B) {
				for i := 0; i < b.N; i++ {
					curOp.fun(val1, val2)
				}
			})
		}
	}
}
