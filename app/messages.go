// Package main
package main

import "fmt"

// -----------------------------------------------------------------------------

// displayHeader displays the name of the program.
// It is used as header for some messages for user.
func displayHeader() {
	fmt.Println(`

	 ██████╗ █████╗ ██╗      ██████╗    ██╗   ██╗ ██╗    ██████╗ 
	██╔════╝██╔══██╗██║     ██╔════╝    ██║   ██║███║   ██╔═████╗
	██║     ███████║██║     ██║         ██║   ██║╚██║   ██║██╔██║
	██║     ██╔══██║██║     ██║         ╚██╗ ██╔╝ ██║   ████╔╝██║
	╚██████╗██║  ██║███████╗╚██████╗     ╚████╔╝  ██║██╗╚██████╔╝
	╚═════╝╚═╝  ╚═╝╚══════╝ ╚═════╝      ╚═══╝   ╚═╝╚═╝ ╚═════╝

	Thank you for using CALC v1.0.0

	`)
}

// DisplayVersion displays the current version of the program.
func DisplayVersion() {

	displayHeader()
	fmt.Println(`

	Current version : 1.0.0

	`)

}

// DisplayHelp displays help for the usage in terminal.
func DisplayHelp() {

	displayHeader()
	fmt.Println(`
	
	usage: calc    ["your_calculation"] ex : calc "40+2"
	               [-v] [-version] [--version] 
	               [-h] [-help] [--help]

	These are operators you can use : + - / * ^ ()

	Limits of CALC V10:
				Max number : 1.7976931348623157e+308
				Min number : -1.7976931348623157e+308
				Decimal precision : 13 digits

	`)

}

// DisplayResult
func DisplayResult(res float64) {
	fmt.Println(res)
}

// DisplayWarning
func DisplayWarning(err error) {
	fmt.Println(err)
}
