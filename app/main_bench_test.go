package main

import (
	"fmt"
	"testing"
)

// BenchmarkGetResult do performances tests for the main function of the application
func BenchmarkGetResult(b *testing.B) {

	tables := []struct {
		input string
	}{

		// Basic tests
		{"-1"},
		{"1+1"},
		{"1*10"},
		{"-0.5*0.5"},
		{"-0.25*(-0.5^10)"},

		// Advanced tests
		{"(-5+(5^5))/55.5*2"},
		{"10e20*5.1542"},
		{"-10    +5 - 42*(2^2) - 10"},
		{"-10e21    +5 - 42*(2^2) - 10"},
		{"-2e37+55 - 142*(22^10) - 10"},
	}

	for _, table := range tables {

		b.Run(fmt.Sprintf(" GetResult / input : %s", table.input), func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				getResult(table.input)
			}
		})
	}

}
