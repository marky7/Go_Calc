// Package main is a calculator to use from a terminal.
// It parses an expression from a string and return the result of the calculation.
package main

import (
	"errors"
	"go/ast"
	"go/parser"
	"os"
	"reflect"
	"strconv"
)

// main function check the number of parameters and execute command if this one is valid.
func main() {
	var input string
	if len(os.Args) == 1 {
		StopProgram(errors.New("The expression is missing. Please check the Syntax. (calc -h or calc --help) "))
	} else if len(os.Args) > 2 {
		StopProgram(errors.New("There are too many arguments. Please check the Syntax. (calc -h or calc --help) "))
	} else {
		input = os.Args[1]
		execCommand(input)
		os.Exit(0)
	}
}

// execCommand take string command as parameter and call the good behaviour.
// the default behaviour is to take the expression as paramater (ex: "-2+2/10")
func execCommand(input string) {
	switch input {

	case "-h", "-help", "--help":
		DisplayHelp()

	case "-v", "-version", "--version":
		DisplayVersion()

	default:
		res, err := getResult(input)
		if err != nil {
			StopProgram(err)
		}
		DisplayResult(res)
	}
}

// getResult takes the expression as string parameter
// It returns the result has float64 or return error
func getResult(input string) (float64, error) {
	//addParToPowNum(&input)
	tr := parseExprAndCreateAST(input)
	return getResultBybrowsingAST(&tr)
}

// StopProgram is called when something went wrong.
// It displays the error for user and stop program.
func StopProgram(err error) {
	DisplayWarning(err)
	os.Exit(1)
}

// parseExprAndCreateAST takes the expression in parameter as string
// It parses the expression to create an AST (Abstract Syntax Tree) that will be traversed and interpreted later.
// documentation of AST : https://golang.org/src/go/ast/ast.go
func parseExprAndCreateAST(input string) ast.Expr {
	tr, errParseExpr := parser.ParseExpr(input)
	if errParseExpr != nil {
		StopProgram(errParseExpr)
	}
	return tr
}

// getResultBybrowsingAST takes the node of the AST (Abstract Syntax Tree) as parameter to traverse and interprete it.
// This method is called recursively do traverse each node of the expression and calculate the final result.
func getResultBybrowsingAST(n *(ast.Expr)) (float64, error) {

	switch d := (*n).(type) {

	case *ast.BasicLit:
		// A BasicLit node represents a literal of basic type.
		return strconv.ParseFloat(d.Value, 64)

	case *ast.ParenExpr:
		// A ParenExpr node represents a parenthesized expression.
		return getResultBybrowsingAST(&(d.X))

	case *ast.BinaryExpr:
		// A BinaryExpr node represents a binary expression.
		// Get X and Y and calculate the result.
		operator := d.Op

		if resX, errX := getResultBybrowsingAST(&(d.X)); errX != nil {
			return resX, errX
		} else {
			if resY, errY := getResultBybrowsingAST(&(d.Y)); errY != nil {
				return resY, errY
			} else {
				return SetOperation(resX, resY, operator)
			}
		}

	case *ast.UnaryExpr:
		// A UnaryExpr node represents a unary expression.
		// Unary "*" expressions are represented via StarExpr nodes.
		// The number has its own operator (ex:-1,+2,etc..).
		operator := d.Op

		if resX, errX := getResultBybrowsingAST(&(d.X)); errX != nil {
			return resX, errX
		} else {
			return SetOperation(0, resX, operator)
		}

	default:
		return 0, errors.New("There is operator(s) unsupported by Go_Calc " + reflect.TypeOf(d).Name())
	}
}
