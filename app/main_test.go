package main

import (
	"testing"
)

// TestGetResult do unit tests for the main function of the application
func TestGetResult(t *testing.T) {

	tables := []struct {
		input  string
		result float64
	}{

		// Basic tests
		{"-1", -1},
		{"1+1", 2},
		{"1*10", 10},
		{"-0.5*0.5", -0.25},
		{"-0.25*(-0.5^10)", -0.000244140625},

		// Advanced tests
		{"(-5+(5^5))/55.5*2", 112.43243243243244},
		{"10e20*5.1542", 5.1542e+21},
		{"-10    +5 - 42*(2^2) - 10", -183},
		{"-10e21    +5 - 42*(2^2) - 10", -1e+22},
		{"-2e37+55 - 142*(22^10) - 10", -2e+37},
	}

	for _, table := range tables {
		total, _ := getResult(table.input)
		if total != table.result {
			t.Errorf("getResult of (%s) was incorrect, got: %g, want: %g.", table.input, total, table.result)
		}
	}

}
