// Package main
package main

import (
	"errors"
	"go/token"
	"math"
	"strconv"
)

// -----------------------------------------------------------------------------

const (
	round float64 = 1e14                     // round defines the maximum precision that is currently possible
	max   float64 = 1.7976931348623157e+308  // max defines the maximum value that it is currently possible to obtain
	min   float64 = -1.7976931348623157e+308 // min defines the minimum value that it is currently possible to obtain
)

// SetOperation detect the available operator and deilimtor in order to redirect to the good calculation
func SetOperation(X float64, Y float64, operator token.Token) (float64, error) {

	switch operator {
	case 15: // QUO -> /
		return Quo(X, Y), nil
	case 13: // SUB -> -
		return Sub(X, Y), nil
	case 12: // ADD -> +
		return Add(X, Y), nil
	case 14: // MUL -> *
		return Mul(X, Y), nil
	case 19: // Pow -> ^
		return Pow(X, Y), nil
	default:
		return 0, errors.New("This operator can't be used : " + strconv.Itoa(int(operator)))
	}
}

// Quo divides X by Y
func Quo(X, Y float64) (result float64) {
	return GoRound(X / Y)
}

// Sub subtracts from X and Y
func Sub(X, Y float64) (result float64) {
	return GoRound(X - Y)
}

// Add performs an addition of X and Y
func Add(X, Y float64) (result float64) {
	return GoRound(X + Y)
}

// Mul performs a multiplication of X and Y
func Mul(X, Y float64) (result float64) {
	return GoRound(X * Y)
}

// Pow returns x**y, the base-x exponential of y.
func Pow(X, Y float64) (result float64) {
	return GoRound(math.Pow(X, Y))
}

// goround allows to round the numbers to n decimal places
// The goal is to to remove the native operations error on the float64 type
// example : 3.2 - 3.0 = 0.20000000000000018
func GoRound(val float64) (result float64) {

	limitRound := (max / round)
	if val < limitRound && val > -1*limitRound {
		return math.Round((val)*round) / round
	}

	return val
}
