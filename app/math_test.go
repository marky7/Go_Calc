package main

import (
	"go/token"
	"testing"
)

func TestSetOperation(t *testing.T) {

	tables := []struct {
		x        float64
		y        float64
		operator token.Token // The Token is type of int
		n        float64
	}{
		// Basic tests
		{1, 1, 12, 2},                   // x + y = n
		{-5.5, 2, 12, -3.5},             // x + y = n
		{1, 2, 13, -1},                  // x - y = n
		{1e55, 1.5e5, 13, 1e+55},        // x - y = n
		{1e60, -2.2, 14, -2.2e60},       // x * y = n
		{1e10, 2, 14, 2e10},             // x * y = n
		{2500, 2, 15, 1250},             // x / y = n
		{-25, 4000, 15, -0.00625},       // x / y = n
		{12, 0.5, 19, 3.46410161513775}, // x ^ y = n
		{10, 40, 19, 1e+40},             // x ^ y = n
	}

	for _, table := range tables {
		total, _ := SetOperation(table.x, table.y, table.operator)
		if total != table.n {
			t.Errorf("SetOperation of (%g %s %g) was incorrect, got: %g, want: %g.", table.x, table.operator, table.y, total, table.n)
		}
	}

}

// TestAdd test the method Add (ex:1+1) with specific values
func TestAdd(t *testing.T) {

	tables := []struct {
		x float64
		y float64
		n float64
	}{
		// Basic numbers test
		{1, 1, 2},
		{1, 2, 3},
		{2, 2, 4},
		{5, 2, 7},

		// Decimal numbers test
		{1.111, 1.222, 2.333},
		{2.333, 2.777, 5.110},
		{2.000, -2.000, 0},
		{0.0110999999999, 0.0110999999999, 0.0221999999998},

		// Big and Small numbers test
		{10e20, 10e20, 2e+21},
		{-10e20, -10e20, -2e+21},
		{-22e40, +22e40, 0},
		{-22e40, 0.0111000, -22e+40},
	}

	for _, table := range tables {
		total := Add(table.x, table.y)
		if total != table.n {
			t.Errorf("Add of (%g+%g) was incorrect, got: %g, want: %g.", table.x, table.y, total, table.n)
		}
	}
}

// TestSub test the method Sub (ex:1-1) with specific values
func TestSub(t *testing.T) {

	tables := []struct {
		x float64
		y float64
		n float64
	}{
		// Basic tests
		{1, 1, 0},
		{1, 2, -1},
		{2, 2, 0},
		{5, 2, 3},

		// Decimal tests
		{1.111, 1.222, -0.111},
		{2.333, 2.777, -0.444},
		{2.000, -2.000, 4},
		{5, 5.0111, -0.0111},

		// Big and Small numbers test
		{10e20, 10e20, 0},
		{-10e20, -10e20, 0},
		{-22e40, +22e40, -4.4e+41},
		{-22e40, 0.0111000, -2.2e+41},
	}

	for _, table := range tables {
		total := Sub(table.x, table.y)
		if total != table.n {
			t.Errorf("Sub of (%g-%g) was incorrect, got: %g, want: %g.", table.x, table.y, total, table.n)
		}
	}
}

// TestQuo test the method Quo (ex:1/1) with specific values
func TestQuo(t *testing.T) {

	tables := []struct {
		x float64
		y float64
		n float64
	}{
		// Basic tests
		{1, 1, 1},
		{1, 2, 0.5},
		{2, 2, 1},
		{5, 2, 2.5},

		// Decimal tests
		{1.111, 1.222, 0.90916530278232},
		{2.333, 2.777, 0.84011523226503},
		{2.000, -2.000, -1},
		{5, 5.0111, 0.99778491748319},

		// Big and Small numbers test
		{10e20, 10e20, 1},
		{-10e20, -10e20, 1},
		{-22e40, +22e41, -0.1},
		{-22e40, 0.0111000, -1.9819819819819817e+43},

		// Max Numbers
		{1.79769313486231e+308, 1, 1.79769313486231e+308},
		{1.79769313486231e+308, 10, 1.79769313486231e+307},
		{1.79769313486231e+308, 10e1, 1.79769313486231e+306},
		{1.79769313486231e+308, 10e2, 1.79769313486231e+305},
		{1.79769313486231e+308, 10e3, 1.79769313486231e+304},
		{1.79769313486231e+308, 10e4, 1.79769313486231e+303},
		// {1.79769313486231e+308, 10e5, 1.79769313486231e+302}, // Error : Native method Pow from Go return 1.7976931348623097e+302 instead of 1.79769313486231e+302
		// {1.79769313486231e+308, 10e6, 1.79769313486231e+301}, // Error : Native method Pow from Go return 1.7976931348623099e+301 instead of 1.79769313486231e+308
		{1.79769313486231e+308, 10e7, 1.79769313486231e+300},
		{1.79769313486231e+308, 10e8, 1.79769313486231e+299},
		{1.79769313486231e+308, 10e9, 1.79769313486231e+298},
		{1.79769313486231e+308, 10e10, 1.79769313486231e+297},
		{1.79769313486231e+308, 10e11, 1.79769313486231e+296},
		// {1.79769313486231e+308, 10e12, 1.79769313486231e+295}, // Error : Native method Pow from Go return 1.7976931348623098e+295 instead of 1.79769313486231e+295
	}

	for _, table := range tables {
		total := Quo(table.x, table.y)
		if total != table.n {
			t.Errorf("Quo of (%g/%g) was incorrect, got: %g, want: %g.", table.x, table.y, total, table.n)
		}
	}
}

// TestMul test the method Mul (ex:1*1) with specific values
func TestMul(t *testing.T) {

	tables := []struct {
		x float64
		y float64
		n float64
	}{
		// Basic tests
		{1, 1, 1},
		{1, 2, 2},
		{2, 2, 4},
		{5, 2, 10},

		// Decimal tests
		{1.111, 1.222, 1.357642},
		{2.333, 2.777, 6.478741},
		{2.000, -2.000, -4},
		{5, 5.0111, 25.0555},

		// Big and Small numbers test
		{10e20, 10e20, 1e+42},
		{-10e20, -10e20, 1e+42},
		{-22e40, +22e41, -4.84e+83},
		{-22e40, 0.0111000, -2.442e+39},

		// Max Numbers
		{1.79769313486231e+306, 1, 1.79769313486231e+306},
		{1.79769313486231e+305, 10, 1.79769313486231e+306},
		{1.79769313486231e+304, 10e1, 1.79769313486231e+306},
		{1.79769313486231e+303, 10e2, 1.79769313486231e+306},
		{1.79769313486231e+302, 10e3, 1.79769313486231e+306},
		{1.79769313486231e+301, 10e4, 1.79769313486231e+306},
		{1.79769313486231e+300, 10e5, 1.79769313486231e+306},
		{1.79769313486231e+299, 10e6, 1.79769313486231e+306},
		{1.79769313486231e+298, 10e7, 1.79769313486231e+306},
		{1.79769313486231e+297, 10e8, 1.79769313486231e+306},
		{1.79769313486231e+296, 10e9, 1.79769313486231e+306},
		{1.79769313486231e+295, 10e10, 1.79769313486231e+306},
		{1.79769313486231e+294, 10e11, 1.79769313486231e+306},
		{1.79769313486231e+293, 10e12, 1.79769313486231e+306},
	}

	for _, table := range tables {
		total := Mul(table.x, table.y)
		if total != table.n {
			t.Errorf("Mul of (%g*%g) was incorrect, got: %g, want: %g.", table.x, table.y, total, table.n)
		}
	}
}

// TestMul test the method Pow (ex:1^1) with specific values
func TestPow(t *testing.T) {

	tables := []struct {
		x float64
		y float64
		n float64
	}{
		// Basic tests
		{1, 1, 1},
		{1, 2, 1},
		{20, 2, 400},
		{-5, 2, 25},

		// Decimal tests
		{1.111, 1.222, 1.13726737339542},
		{2.333, 2.777, 10.5123359050564},
		{2.000, -2.000, 0.25},
		{5, 5.0111, 3181.3290318200575},

		// Big and Small numbers test
		{10, -10, 0.0000000001},
		{10, -100, 0},
		{-10e20, -10, 0},
		{10e20, 2, 1e+42},
		{1020, 99, 7.102594233580719e+297},
		{-1020, 100, 7.244646118252334e+300},

		// {-22e40, +22e41, math.Inf(-1)}, 			// Error : Native method Pow from Go return +infini au lieux de -infini
		// {-22e40, 0.5, -469041575982342955500}, 	// Error : Native method Pow from Go return NaN
		// {-22e40, -25, -8.3068637e-94}, 			// Error : Native method Pow from Go return -0
	}

	for _, table := range tables {
		total := Pow(table.x, table.y)
		if total != table.n {
			t.Errorf("Pow of (%g^%g) was incorrect, got: %g, want: %g.", table.x, table.y, total, table.n)
		}
	}
}

// TestGoRound test the method GoRound because Golang has some issues with float64 number operations (ex:3.2 - 3.0 = 0.20000000000000018)
func TestGoRound(t *testing.T) {

	tables := []struct {
		value  float64
		result float64
	}{
		// Basic tests
		{0.20000000000000018, 0.2},
		{0.200000000000009, 0.20000000000001},
		{99999999999990.200000000000009, 99999999999990.20000000000001},
		{99999999999990.20000000000000015, 99999999999990.20000000000000015},
		{9999999999999999999999999999999999999999990.20000000000000009, 9999999999999999999999999999999999999999990},
		{1.79769313486231e+308, 1.79769313486231e+308},
		{-1.79769313486231e+308, -1.79769313486231e+308},
		{1.79769313486231e+307, 1.79769313486231e+307},
		{-1.79769313486231e+307, -1.79769313486231e+307},
	}

	for _, table := range tables {
		resRound := GoRound(table.value)
		if resRound != table.result {
			t.Errorf("GoRound of %g was incorrect, got: %g, want: %g.", table.value, resRound, table.result)
		}
	}
}
